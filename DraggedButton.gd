extends Sprite

signal released(where)

var isAccepting = false;
var accTex = preload("res://accepting.png");
var refTex = preload("res://inactive.png");
# Called when the node enters the scene tree for the first time.
func _ready():
	if(isAccepting):
		texture = accTex;
	else:
		texture = refTex;
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mouse_pos = get_viewport().get_mouse_position();
	var sprite = self;
	var displacement = sprite.texture.get_size();
	displacement.x*=sprite.get_scale().x;
	displacement.y*=sprite.get_scale().y;
	var newPosition = mouse_pos+displacement/2;
	if(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		position = newPosition;
	else: 
		emit_signal("released", newPosition, isAccepting)
		queue_free()
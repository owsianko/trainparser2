extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func random(_min, _max):
	return randi() % (_max - _min + 1) + _min

func even(val):
	if (val % 2):
		return val - 1
	else:
		return val

func sum(v):
	var res = 0
	for n in v:
		res += n
	return res

func generate_occurences(zeros, length):
	var occurences = [zeros, length - zeros]
	return occurences

func generate_random_occurences(length):
	var zeros = random(0, length)
	return generate_occurences(zeros, length)

func helper_generate_string(sub_res, sub_length, occurences, sigma):
	var size = sigma.size()
	if (!sub_length):
		return sub_res
	var i = random(0, size - 1)
	while !occurences[i]:
		i = random(0, size - 1)
	occurences[i] -= 1
	helper_generate_string(sub_res + sigma[i], sub_length - 1, occurences, sigma)

func generate_string(sigma, occurences):
	var length = sum(occurences)
	return helper_generate_string("", length, occurences, sigma)

func generate_01_string(occurences):
	return generate_string(['0', '1'], occurences)

func helper_generate_level_3_valid(sub_res, sub_length, sigma):
	if (!sub_length):
		return sub_res
	var i
	if (sub_res.substr(sub_res.size() - 1, 1) == '0'):
		i = 1
	else:
		i = random(0, 1)
	helper_generate_level_3_valid(sub_res.append(sigma[i]), sub_length - 1, sigma)
 
func generate_level_3_valid(length):
	return helper_generate_level_3_valid("", length, [0, 1])

###########
# Level 1 #
###########

func two_zeros(length):
	if (length < 2):
	    length = 2
	var occurences = generate_occurences(2, length)
	return generate_01_string(occurences)

func one_zero(length):
	if (!length):
	    length = 1
	var occurences = generate_occurences(1, length)
	return generate_01_string(occurences)


func no_zeros(length):
	if (!length):
	    length = 1
	var occurences = generate_occurences(0, length)
	return generate_01_string(occurences)

func even_zeros_no_ones(length):
	if (length < 4):
	    length += 2
	length = even(length)
	var occurences = generate_occurences(length, length)
	return generate_01_string(occurences)

func odd_zeros(length):
	if (length < 3):
	    length = 3
	var zeros = even(random(2, length - 1)) + 1
	var occurences = generate_occurences(zeros, length)
	return generate_01_string(occurences)

func even_zeros(length):
	if (length < 6):
	    length = 6
	var zeros = even(random(4, length - 1))
	var occurences = generate_occurences(zeros, length)
	return generate_01_string(occurences)

###########
# Level 2 #
###########

func ends_in_10(length):
	if (length < 2):
		length = 2
	length -= 2
	var occurences = generate_random_occurences(length)
	return generate_01_string(occurences) + "10"

func ends_in_100(length):
	if (length < 3):
		length = 3
	length -= 3
	var occurences = generate_random_occurences(length)
	return generate_01_string(occurences) + "100"

func ends_in_even_zeros(length):
	if (length < 5):
		length = 5
	length -= 1
	var end_zeros = random(2, length)
	length -= end_zeros
	var occurences = generate_random_occurences(length)
	return generate_01_string(occurences) + "1" + even_zeros_no_ones(end_zeros)

# helper
func odd_zeros_no_ones(length):
	if (length < 1):
		length = 1
	length = even(length) + 1
	var occurences = generate_occurences(length, length)
	return generate_01_string(occurences)

func ends_in_odd_zeros(length):
	if (length < 4):
		length = 4
	length -= 1
	var end_zeros = random(3, length)
	length -= end_zeros
	var occurences = generate_random_occurences(length)
	return generate_01_string(occurences) + "1" + odd_zeros_no_ones(end_zeros)

func ends_in_ones(length):
	if (length < 2):
		length = 2
	length -= 1
	var end_ones = random(1, length)
	length -= end_ones
	var occurences = generate_random_occurences(length)
	return generate_01_string(occurences) + "0" + no_zeros(end_ones)

###########
# Level 3 #
###########

func contains_100_once(length):
	if (length < 3):
		length = 3
	length -= 3
	var prefix_length = random(0, length)
	length -= prefix_length
	return generate_level_3_valid(prefix_length) + "100" + generate_level_3_valid(length)

func contains_100_at_least_once(length):
	if (length < 3):
		length = 3
	length -= 3
	var prefix_length = random(0, length)
	var prefix_occurences = generate_random_occurences(prefix_length)
	length -= prefix_length
	var occurences = generate_random_occurences(length)
	return generate_01_string(prefix_occurences) + "100" + generate_01_string(occurences)

func ends_with_random_zeros(length):
	if (length < 1):
		length = 1
	var zeros = random(1, length)
	var occurences = generate_occurences(zeros, zeros)
	length -= zeros
	var prefix_occurences = generate_random_occurences(length)
	return generate_01_string(prefix_occurences) + generate_01_string(occurences)
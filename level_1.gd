extends "res://levels.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var strings = level_inputs();
	for pair in strings:
		if pair[1]:
			accepted.append(pair[0])
		else:
			rejected.append(pair[0])

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func level_inputs():
	var v = [
		[two_zeros(random(2, 10)), true],
		[one_zero(random(1, 10)), false], 
		[no_zeros(random(1, 10)), true], 
		[even_zeros_no_ones(random(4, 10)), true],
		[odd_zeros(random(3, 10)), false],
		[even_zeros(random(6, 10)), true]]
	return v

var accepted = []
var rejected = []

func randomAcceptedString():
	var i = random(0, accepted.size() - 1)
	return accepted[i]
	
func randomRejectedString():
	var i = random(0, rejected.size() - 1)
	return rejected[i]
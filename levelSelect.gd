extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

const levels = {
	1 : preload("res://testLevel.gd"),
	2 : preload("res://level_1.gd"),
	3 : preload("res://level_2.gd"),
	4 : preload("res://level_3.gd")
}


func _on_Button_pressed():
	goToLevel(1)


func _on_Button3_pressed():
	goToLevel(3)


func _on_Button2_pressed():
	goToLevel(2)


func _on_Button4_pressed():
	goToLevel(4)

func goToLevel(num):
	var root = get_parent()
	var here = root.get_node("levelSelect");
	root.remove_child(here);
	here.call_deferred("ree");
	
	var level = load("res://DFASpace.tscn").instance();
	level.level = levels[num].new()
	root.add_child(level);
extends Node2D

signal pressed
signal outskirtPressed
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var isAccepting = false;
var isStart = false;
var hasSelfConnection = false;
var accTex = preload("res://accepting.png");
var rejTex = preload("res://rejecting.png");
var accRingTex = preload("res://accepting_rails.png");
var rejRingTex = preload("res://rejecting_rails.png");
var currSymbols = []
# Called when the node enters the scene tree for the first time.
func _ready():
	updateSprite()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func updateSprite():	
	if isAccepting:
		if hasSelfConnection:
			$Sprite.texture = accRingTex
		else:
			$Sprite.texture = accTex
	else:
		if hasSelfConnection:
			$Sprite.texture = rejRingTex
		else:
			$Sprite.texture = rejTex
			
	if isStart:
		$flag.show()
	else:
		$flag.hide()
		
	if hasSelfConnection or true:
		var txt = "";
		for i in range(currSymbols.size()):
			if i > 0:
				txt+=", "
			txt+=currSymbols[i]
		$Label.text = txt
		$Label.show()
	else:
		$Label.hide()
	
func setSymbols(symbols):
	currSymbols = symbols
	updateSprite()

func toggleStart(value):
	isStart = value
	updateSprite()

func toggleRails(value):
	hasSelfConnection = value;
	updateSprite()
		

func _on_TextureButton_button_down():
	emit_signal("pressed", self)

func contains(where):
	var texture_width = $TextureButton.texture_normal.get_width() * scale.x
	var texture_height = $TextureButton.texture_normal.get_height() * scale.y
	var inHor = position.x-texture_width/2 < where.x && position.x + texture_width/2 > where.x
	var inVer = position.y-texture_height/2 < where.y && position.y + texture_height/2 > where.y
	return inHor and inVer


func _on_TextureButton2_pressed():
	emit_signal("outskirtPressed", self);

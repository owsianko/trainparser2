extends Sprite
signal released(where)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mouse_pos = get_viewport().get_mouse_position();
	var displacement = texture.get_size();
	displacement.x*=get_scale().x;
	displacement.y*=get_scale().y;
	var newPosition = mouse_pos-displacement;
	if(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		position = newPosition;
	else: 
		emit_signal("released", position)
		queue_free()


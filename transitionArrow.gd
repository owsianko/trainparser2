extends "res://arrow.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var currSymbols = [];

# Called when the node enters the scene tree for the first time.
func _ready():
	setSymbols([])

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Label.rect_scale = Vector2(2,2)/scale # Constant scale for the text
	var actualLabelSize = $Label.rect_size*$Label.rect_scale
	$Label.rect_position = -actualLabelSize
	$Label.rect_position.y = $lefthead.rect_position.y*2/3


func _on_body_pressed():
	emit_signal("pressed", self)

func setSymbols(symbols):
	currSymbols = symbols
	var newSymbols = ""
	var i = 0;
	while i < symbols.size():
		if i != 0:
			newSymbols+=","
		newSymbols+=symbols[i]
		i+=1
	
	$Label.text = newSymbols
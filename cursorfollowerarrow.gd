extends "res://arrow.gd"

signal released(where)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var offset = 0;
var origin;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	end = get_viewport().get_mouse_position()-offset
	if(not Input.is_mouse_button_pressed(BUTTON_LEFT)):
		emit_signal("released", end, origin);
		queue_free();

extends "res://levels.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var strings = level_inputs();
	for pair in strings:
		if pair[1]:
			accepted.append(pair[0])
		else:
			rejected.append(pair[0])

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func level_inputs():
	var v = [
		[no_zeros(random(0, 10)), true],
		[generate_level_3_valid(random(0, 10)), true],
		[contains_100_once(random(0, 10)), false],
		[generate_level_3_valid(random(0, 10)), true],
		[contains_100_at_least_once(random(0, 10)), false],
		[ends_with_random_zeros(random(0, 10)), false]];
	return v;
	
var accepted = []
var rejected = []

func randomAcceptedString():
	var i = random(0, accepted.size() - 1)
	return accepted[i]
	
func randomRejectedString():
	var i = random(0, rejected.size() - 1)
	return rejected[i]
extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var lvl = get_node("../..").level
	var strings = lvl.level_inputs()
	var txt = "Here is a set of strings: \n"
	for pair in strings:
		txt += str(pair[0])
		txt += " should be "
		if pair[1]:
			txt += "accepted"
		else:
			txt += "rejected"
		txt+='\n'
	text = txt

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

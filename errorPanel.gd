extends Line2D

var attachedTo = Vector2(50, 50)
var text = "Sample Text"

# Called when the node enters the scene tree for the first time.
func _ready():
	points = [attachedTo,$Panel.rect_position]
	get_node("Panel/Label").text = text



func _on_Button_pressed():
	queue_free()

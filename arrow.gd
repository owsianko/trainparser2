extends Node2D

signal pressed
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var start = Vector2(500,50);
var end = Vector2(1000,100);
var thickness = 100;
var yOffset = 1500;
var textureSize = Vector2(1,1);
var bodyTex = preload("res://rail_centre.png");
var body = [];
# Called when the node enters the scene tree for the first time.
func _ready():
	textureSize.x = $lefthead.texture_normal.get_size().x+$righthead.texture_normal.get_size().x
	textureSize.y = max($lefthead.texture_normal.get_size().y, $righthead.texture_normal.get_size().y)
	$lefthead.rect_position.y-=yOffset;
	$righthead.rect_position.y-=yOffset;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var direction = end-start;
	rotation = direction.angle();
	position = start+direction/2;
	scale.y = thickness/textureSize.y
	scale.x = scale.y
	var baseArrowLength = textureSize.x*scale.x;
	var numToPlace = 0;
	if direction.length() < baseArrowLength*3/4:
		$lefthead.hide()
		$righthead.hide()
	else:
		var headWidth = $lefthead.texture_normal.get_size().x
		var bodyWidth = bodyTex.get_size().x
		var extraDist = (direction.length()-baseArrowLength)/(2*scale.x)
		var displacement = headWidth+extraDist
		$lefthead.rect_position.x = -displacement-headWidth/4
		$righthead.rect_position.x = displacement-3*headWidth/4
		$lefthead.show()
		$righthead.show()
		var distToFill = $righthead.rect_position.x-($lefthead.rect_position.x+headWidth)
		numToPlace = ceil(distToFill/bodyWidth);
		for i in range(numToPlace):
			# adding new piece
			if i >= body.size():
				var piece = TextureButton.new()
				piece.texture_normal = bodyTex
				piece.rect_position.y = $lefthead.rect_position.y
				piece.rect_position.x = $lefthead.rect_position.x + headWidth + bodyWidth*i
				piece.connect("pressed", self, "_on_body_pressed")
				body.append(piece)
				add_child(piece)
			# updating existing piece
			else:
				body[i].rect_position.x = $lefthead.rect_position.x + headWidth + bodyWidth*i
				
	# remove unneeded segments
	while body.size() > numToPlace:
		body[-1].queue_free()
		body.pop_back()



func _on_body_pressed():
	emit_signal("pressed", self) 

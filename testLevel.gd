extends "res://levels.gd"

var sigma = ["0", "1"]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func level_inputs():
	var v = [
		['0', true],
		['1', false],
		[odd_zeros(3), false]]
	return v

func randomAcceptedString():
	return [sigma[0]]

func randomRejectedString():
	while true:
		var numThings = randi()%11;
		var result = []
		for i in range(numThings):
			result.append(sigma[randi()%2]);
		if result.size() != 1 or result[0] != sigma[0]:
			return result
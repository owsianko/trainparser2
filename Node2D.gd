extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var start = Vector2(0,0);
var end = Vector2(0,0);
var thickness = 5;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var direction = end-start;
	$body.rotation = direction.angle();
	$body.position = end+direction/2;
	if(direction.length()!=0):
		$body.scale.x = 5.0/direction.length();
	$body.scale.y = thickness/$body.texture.get_size().y

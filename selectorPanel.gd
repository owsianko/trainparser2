extends Panel
signal applied
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var states = {"0": true, "1": false, "2": true}
var buttons = {}

const maxPerColumn = 10
# Called when the node enters the scene tree for the first time.
func _ready():
	var x = 0
	var y = 0
	var maxHeight = 0;
	var maxWidth = 0;
	var offset = 5;
	for state in states.keys():
		var newButton = CheckBox.new()
		newButton.text = state
		add_child(newButton)
		newButton.set_owner(self)
		if newButton.rect_size.x > maxHeight:
			maxHeight = newButton.rect_size.x
		if newButton.rect_size.y > maxWidth:
			maxWidth = newButton.rect_size.y
		buttons[state] = newButton
		newButton.pressed = states[state]
	for button in buttons.values():
		button.margin_top = x*maxHeight + offset
		button.margin_left = y*maxWidth
		x+=1
		y = y+x/maxPerColumn
		x = x%maxPerColumn

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for state in buttons:
		states[state] = buttons[state].pressed

func is_class(type):
	return type == "selectorPanel" or .is_class(type)

func _on_Close_pressed():
	queue_free()


func _on_Apply_pressed():
	emit_signal("applied", states)
	queue_free()

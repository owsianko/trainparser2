extends "res://levels.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var strings = level_inputs();
	for pair in strings:
		if pair[1]:
			accepted.append(pair[0])
		else:
			rejected.append(pair[0])

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func level_inputs():
	var v = [
		[one_zero(1), true],
		[ends_in_10(random(2, 10)), true],
		[ends_in_100(random(3, 10)), false],
		[ends_in_even_zeros(random(5, 10)), false],
		[ends_in_odd_zeros(random(4, 10)), true],
		[ends_in_ones(random(2, 10)), false]]
	return v

var accepted = []
var rejected = []

func randomAcceptedString():
	var i = random(0, accepted.size() - 1)
	return accepted[i]
	
func randomRejectedString():
	var i = random(0, rejected.size() - 1)
	return rejected[i]
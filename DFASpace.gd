extends Area2D
enum OnClickAction {
	NewState,
	NewAcceptingState,
	TransitionAdd,
	TransitionEdit,
	Delete,
	SetStart,
}
const nameDict = {
	OnClickAction.NewState : "Rejecting State Insertion",
	OnClickAction.NewAcceptingState : "Accepting State Insertion",
	OnClickAction.TransitionAdd : "Transition Insertion",
	OnClickAction.TransitionEdit : "Transition editing",
	OnClickAction.Delete : "Deletion",
	OnClickAction.SetStart : "Setting start node",
}
var level = preload("res://testLevel.gd").new()
var startNode = null
# mapping from arrows to their starting states
var arrowStarts = {}
# mapping from arrows to their arrival states
var arrowEnds = {}

var currentStateOrigin;

var onClickMode = OnClickAction.NewState;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
var acceptingScene = preload("res://State.tscn");
var arrowScene = preload("res://transitionArrow.tscn");
var cursorFollowerArrow = preload("res://cursorfollowerarrow.tscn");
var selectorPanel = preload("res://selectorPanel.tscn");
var panicPanel = preload("res://errorPanel.tscn");
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_node("ModePanel/ModeContainer").text = nameDict[onClickMode]

func _on_NodeButtonPressed(which):
	if onClickMode == OnClickAction.TransitionAdd:
		var arr = cursorFollowerArrow.instance();
		arr.start = which.position;
		arr.offset = position;
		arr.origin = which;
		arr.connect("released", self, "_on_arrow_release");
		arr.z_index = -1
		add_child(arr);
	elif onClickMode == OnClickAction.Delete:
		for arrow in get_tree().get_nodes_in_group("arrows"):
			if arrowStarts[arrow] == which or arrowEnds[arrow] == which:
				deleteArrow(arrow)
			
		deleteState(which)
	elif onClickMode == OnClickAction.SetStart:
		if startNode != null:
			startNode.toggleStart(false)
		startNode = which
		startNode.toggleStart(true)

func deleteState(s):
	if startNode == s:
		startNode = null
	s.queue_free()

func _on_NodeOutskirtPressed(which):
	if onClickMode == OnClickAction.TransitionEdit:
		toUpdate = which
		addPanel(which.position, which.currSymbols)

func _on_arrow_release(where, origin):
	var states = get_tree().get_nodes_in_group("states")
	for state in states:
		if(state.contains(where)):
			if(origin!=state):
				var newArrow = arrowScene.instance();
				newArrow.start = origin.position
				newArrow.end = state.position
				newArrow.connect("pressed", self, "_on_arrow_pressed")
				newArrow.add_to_group("arrows")
				newArrow.z_index = -1
				arrowStarts[newArrow] = origin
				arrowEnds[newArrow] = state
				add_child(newArrow)
			else:
				origin.toggleRails(true)
				
			
func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed:
		match onClickMode:
			OnClickAction.NewState:
				newState(false)
			OnClickAction.NewAcceptingState:
				newState(true)
			OnClickAction.TransitionAdd:
				pass # dealt with through on-click

func deleteArrow(arrow):
	arrow.queue_free()
	arrowStarts.erase(arrow)
	arrowEnds.erase(arrow)

func newState(isAcc):
	var newInstance = acceptingScene.instance();
	newInstance.position = get_viewport().get_mouse_position();
	newInstance.isAccepting = isAcc;
	newInstance.connect("pressed", self, "_on_NodeButtonPressed");
	newInstance.connect("outskirtPressed", self, "_on_NodeOutskirtPressed");
	add_child(newInstance);
	newInstance.add_to_group("states");

var toUpdate

func _on_arrow_pressed(arrow):
	if onClickMode == OnClickAction.TransitionEdit:
		toUpdate = arrow
		addPanel(arrow.position, arrow.currSymbols)
	elif onClickMode == OnClickAction.Delete:
		deleteArrow(arrow)

func addPanel(position, transitionSymbols):
	# remove previous panel
	for child in get_children():
		if child.is_class("selectorPanel"):
			child.queue_free()
	var newSelectorPanel = selectorPanel.instance()
	newSelectorPanel.rect_position = position
	newSelectorPanel.connect("applied", self, "selectorPanelApplied")
	var panelStates = {}
	for s in level.sigma:
		panelStates[s] = false
	
	for s in transitionSymbols:
		panelStates[s] = true
	
	newSelectorPanel.states = panelStates
	add_child(newSelectorPanel)	
	
func selectorPanelApplied(state):
	var newSymbols = []
	for s in state:
		if state[s]:
			newSymbols.append(s)
	
	toUpdate.setSymbols(newSymbols);
	

func _on_newR_pressed():
	onClickMode = OnClickAction.NewState


func _on_newA_pressed():
	onClickMode = OnClickAction.NewAcceptingState


func _on_transitionAdd_pressed():
	onClickMode = OnClickAction.TransitionAdd


func _on_reset_pressed():
	var states = get_tree().get_nodes_in_group("states")
	for state in states:
		state.queue_free()
	for arrow in get_tree().get_nodes_in_group("arrows"):
		arrow.queue_free()
	
	arrowStarts = {}
	arrowEnds = {}

func _on_transitionEdit_pressed():
	onClickMode = OnClickAction.TransitionEdit


func _on_checkSolution_pressed():
	var states = get_tree().get_nodes_in_group("states");
	var arrows = get_tree().get_nodes_in_group("arrows");
	
	if startNode == null:
		makePopup("No start node set!")
		return
	# mapping of pointer to integer id
	var stateNumbers = {}
	var numbersState = {}
			
	# dictionary of dictionaries	
	var transitions = {};
	
	var count = 0;
	for state in states:
		stateNumbers[state] = count
		numbersState[count] = state
		var transition = []
		for symbol in state.currSymbols:
			var pair = [symbol, count]
			transition.append(pair)
		transitions[count] = transition
		count+=1

	for arrow in arrows:
		var origin = stateNumbers[arrowStarts[arrow]]
		var destination = stateNumbers[arrowEnds[arrow]]
		for symbol in arrow.currSymbols:
			transitions[origin].append([symbol,destination])
		
	
	var transitionsMap = {}
	var problemText = ""
	var problem = false
	var problemState
	for state in transitions.keys():
		var transition = transitions[state]
		if transition.size() != level.sigma.size():
			problem = true
			problemState = state
			problemText = "Incorrect number of outgoing transitions"
			break
		
		var transitionMap = {}
		for pair in transition:
			var symbol = pair[0]
			var dest = pair[1]
			if transitionMap.has(symbol):
				problem = true;
				problemState = state;
				problemText = str("Duplicate symbol found!", symbol)
				break;
			transitionMap[symbol] = dest;
		
		transitionsMap[state] = transitionMap

	if problem:
		makePopup(problemText, numbersState[problemState].position)
	else:	
		var numSamples = 4000;
		var failedToAccept = [];
		var failedToReject = [];
		for i in range(numSamples):
			var needMoreRejectingSamples = failedToReject.size() < 5
			var needMoreAcceptingSamples = failedToAccept.size() < 5
			if not needMoreAcceptingSamples and not needMoreRejectingSamples:
				break;
			# check for an accepting string
			if (needMoreAcceptingSamples and (randi()%2)==0) or not needMoreRejectingSamples:
				var string = level.randomAcceptedString()
				# if the user-provided dfa rejects the string
				if not checkDFA(transitionsMap, string, stateNumbers[startNode], numbersState):
					failedToAccept.append(string)
			else:
				var string = level.randomRejectedString()
				if checkDFA(transitionsMap, string, stateNumbers[startNode], numbersState):
					failedToReject.append(string);
			
			# gathered enough samples
		if failedToAccept.size() == failedToReject.size() and failedToReject.size() == 0:
			makePopup("Congrats!")
		else:
			var text =""
			if failedToAccept.size() > 0:
				text+= "Your DFA failed to accept the following strings:\n"
				for failed in failedToAccept:
					for c in failed:
						text+=c
					text+='\n'
			if failedToReject.size() > 0:
				text+= "Your DFA failed to reject the following strings:\n"
				for failed in failedToReject:
					for c in failed:
						text+=c
					text+='\n'
			makePopup(text)

func makePopup(txt, where=null):
	var popupsGroup = "popup";
	for popup in get_tree().get_nodes_in_group(popupsGroup):
		popup.queue_free()
		
	var panel = panicPanel.instance()
	panel.add_to_group(popupsGroup)
	panel.attachedTo=where
	panel.text = txt
	add_child(panel)

func _on_delet_pressed():
	onClickMode = OnClickAction.Delete
	
func checkDFA(transitions, string, start, lookup):
	var current = start;
	for c in string:
		current = transitions[current][str(c)]
	return lookup[current].isAccepting

func _on_setStart_pressed():
	onClickMode = OnClickAction.SetStart
